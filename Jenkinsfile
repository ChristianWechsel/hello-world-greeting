node('master'){
	//Abbruch der Pipeline, wenn Pipeline auf master Branch angewendet wird
	if (env.BRANCH_NAME == 'master') {
		currentBuild.result = 'ABORTED'
		error('Not allowed to run pipeline on master branch')
	}
	
	
	def mvnHome
	stage('Preparation') {
		echo '------------------------Stage: Preparation------------------------------------------------------'
		
		//Auswahl in welches Nexus Repo das Artefakt gespeichert werden soll
		//1 = Release Repo
		//2 = Snapshot-Repo
		RepoSelect = 2
		//Es kann nur ein hosted repo in Nexus angegeben werden 
		NEXUS_REPOSITORY_RELEASE = "maven-releases"
		NEXUS_REPOSITORY_SNAPSHOT = "maven-snapshots"
		//Weiche fuer Speichern der Artefakte in Nexus
		if(RepoSelect==1){
			NEXUS_REPOSITORY = "maven-releases"
		}
		if(RepoSelect==2){
			NEXUS_REPOSITORY = "maven-snapshots"
		}
		
		//Konfiguration der lokalen Maven Installation
		mvnHome = tool 'MAVEN_HOME'
		
		//Beziehen der Daten aus Branch welche in Bitbucket liegen
		checkout scm
		
		//Variablen definieren welche f�r NEXUS notwendig sind
		NEXUS_VERSION = "nexus3"
		NEXUS_PROTOCOL = "http"
		NEXUS_URL = "localhost:8081"
		
		//Es wird die ID aus Jenkins-Verwalung der Zugangsdaten verwendet
		NEXUS_CREDENTIAL_ID = "nexus-credentials"
    }
	
	stage('Build & Unit Test') {
		echo '------------------------Stage: Build & Unit Test------------------------------------------------------'
		
		//Projekt mit Maven bauen
		bat(/"${mvnHome}\bin\mvn" -Dmaven.test.failure.ignore clean verify/) 
		junit '**/target/surefire-reports/TEST-*.xml'
		archiveArtifacts 'target/*.war'
	}
	
	stage('Static Code Analysis'){
		echo '------------------------Stage: Static Code Analysis------------------------------------------------------'
		
		bat(/"${mvnHome}\bin\mvn" -Dmaven.test.failure.ignore clean verify sonar:sonar -Dsonar.projectName=example-project -Dsonar.projectKey=example-project -Dsonar.projectVersion=$BUILD_NUMBER';/)
	}
    
	stage('Integration Test'){
		echo '------------------------Stage: Integration Test------------------------------------------------------'

		//aktueller Branch (muss eingestellt werden) mit master mergen
		//Parameter $class: 'LocalBranch' ist erforderlich, da sonst lokaler Branch "detached" wird und kein push m�glich ist
		checkout([$class: 'GitSCM', branches: [[name: '*/DevMerge']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout'], [$class: 'LocalBranch'], [$class: 'PreBuildMerge', options: [fastForwardMode: 'NO_FF', mergeRemote: 'origin', mergeTarget: 'master']]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'SSH-Bitbucket', url: 'https://ChristianWechsel@bitbucket.org/ChristianWechsel/hello-world-greeting.git']]])
		
		//Projekt mit Maven bauen
		bat(/"${mvnHome}\bin\mvn" -Dmaven.test.failure.ignore clean verify -Dsurefire.skip=true';/)
		junit '**/target/surefire-reports/TEST-*.xml'
		archiveArtifacts 'target/*.war'
	}
	
	stage('Push to Remote'){
		echo '------------------------Stage: Push to Remote-----------------------------------------------------'

		bat label: '', script: '''git add --all
		git commit -m "Jenkins: push to remote"
		git push -u origin master
		'''
	}
     
	stage('Publish') {
		echo '------------------------Stage: Publish------------------------------------------------------'
		
		// Read POM xml file using 'readMavenPom' step , this step 'readMavenPom' is included in: https://plugins.jenkins.io/pipeline-utility-steps
		pom = readMavenPom file: "pom.xml";
		// Find built artifact under target folder
		filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
		// Print some info from the artifact found
		echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
		// Extract the path from the File found
		artifactPath = filesByGlob[0].path;
		// Assign to a boolean response verifying If the artifact name exists
		artifactExists = fileExists artifactPath;
		
		if(artifactExists){
			echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";
			nexusArtifactUploader(
			nexusVersion: NEXUS_VERSION,
			protocol: NEXUS_PROTOCOL,
			nexusUrl: NEXUS_URL,
			groupId: pom.groupId,
			version: pom.version,
			repository: NEXUS_REPOSITORY,
			credentialsId: NEXUS_CREDENTIAL_ID,
				artifacts: [
                	// Artifact generated such as .jar, .ear and .war files.
					[artifactId: pom.artifactId,
                    classifier: '',
                    file: artifactPath,
                    type: pom.packaging],
						// Lets upload the pom.xml file for additional information for Transitive dependencies
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
					]          
			);
		} else {
			error "*** File: ${artifactPath}, could not be found";
		}
	} 	
}
