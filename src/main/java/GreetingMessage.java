package hello;

public class GreetingMessage {
  
     private String message;
     private int hours;
	 private final int lunchtime = 12;
	 private final int dinner = 17;

     // constructor
     public GreetingMessage(){
      
      DateTime var = new DateTime();
        this.hours = var.dateTime();

      if (hours < lunchtime)
      this.message = "Good Morning!";
      else if (hours < dinner && !(hours == lunchtime))
      this.message = "Good Afternoon!";
      else if (hours == lunchtime)
      this.message = "Good Noon!";
      else
      this.message = "Good Evening!";
     }
      
     // return message 
     public String printMessage(){
        System.out.println(message);
        return message;
     }   
  } 
